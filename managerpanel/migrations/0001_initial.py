# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contact'
        db.create_table(u'managerpanel_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mobile', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('home', self.gf('django.db.models.fields.CharField')(max_length=12)),
        ))
        db.send_create_signal(u'managerpanel', ['Contact'])

        # Adding model 'ZManager'
        db.create_table(u'managerpanel_zmanager', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('zostel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Zostel'])),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['managerpanel.Contact'])),
        ))
        db.send_create_signal(u'managerpanel', ['ZManager'])

        # Adding model 'Transaction'
        db.create_table(u'managerpanel_transaction', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transaction_id', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('zostel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Zostel'])),
            ('zmanager', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['managerpanel.ZManager'])),
            ('added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=2)),
            ('note', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('transaction_type', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'managerpanel', ['Transaction'])


    def backwards(self, orm):
        # Deleting model 'Contact'
        db.delete_table(u'managerpanel_contact')

        # Deleting model 'ZManager'
        db.delete_table(u'managerpanel_zmanager')

        # Deleting model 'Transaction'
        db.delete_table(u'managerpanel_transaction')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'booking.city': {
            'Meta': {'object_name': 'City'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'booking.zostel': {
            'Meta': {'object_name': 'Zostel'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'city': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['booking.City']", 'unique': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'managerpanel.contact': {
            'Meta': {'object_name': 'Contact'},
            'home': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'managerpanel.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'transaction_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'zmanager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['managerpanel.ZManager']"}),
            'zostel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Zostel']"})
        },
        u'managerpanel.zmanager': {
            'Meta': {'object_name': 'ZManager'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['managerpanel.Contact']"}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'zostel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Zostel']"})
        }
    }

    complete_apps = ['managerpanel']