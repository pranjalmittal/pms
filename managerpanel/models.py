from django.contrib.auth.models import User
from django.db import models
from booking.models import Zostel, City

CHOICES = (
        ('Revenue', 'R'),
        ('Expense', 'E'),
)

class Contact(models.Model):
    mobile = models.CharField(max_length=10)
    home = models.CharField(max_length=12)

class ZManager(models.Model):
    '''
    User who is a manager
    '''
    user = models.OneToOneField(User)
    name = models.CharField(max_length=50)
    zostel = models.ForeignKey(Zostel)
    email = models.CharField(max_length = 50)
    contact = models.ForeignKey(Contact)

    def __unicode__(self):
        return u'%s %s' % (self.name, self.zostel.name)

class Transaction(models.Model):
    '''
    Every Transaction is associated with one zostel property and one Manager
    '''
    transaction_id = models.CharField(max_length=5)
    zostel = models.ForeignKey(Zostel)
    zmanager = models.ForeignKey(ZManager)
    added_on = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    note = models.CharField(max_length=100)
    transaction_type = models.CharField(max_length=20, choices=CHOICES)

    def __unicode__(self):
        return self.transaction_id