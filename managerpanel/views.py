from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.template import RequestContext
from django.http import HttpResponse, Http404

from .models import *
from .forms import TransactionForm

###########################################
# Check manager 
###########################################
def manager_login_required(view_function):
    '''
    A decorator, that can be used around any view function to check if the user is a manager
    '''
    def wrapper(request, *args, **kwargs):
        manager = ZManager.objects.get(user=request.user.id)
        if manager:
            return view_function(request, *args, **kwargs)
        else:
            return HttpResponse("Not Authorized")
    return wrapper

###########################################
# Main Views
###########################################

@manager_login_required
def manager_panel(request):
    return render(request,'dashboard.html')

@manager_login_required
def add_transaction(request):
    if request.method == 'GET':
        form = TransactionForm()
    else:
        # A POST request
        # Bind data from request.POST
        form = TransactionForm(request.POST)
        if form.is_valid():
            zostel = form.cleaned_data['Zostel']
            zmanager = form.cleaned_data['Manager']
            amount = form.cleaned_data['Amount']
            note = form.cleaned_data['Note']
            transaction_type = form.cleaned_data['']
            transaction = Transaction.objects.create(zostel=zostel, zmanager=zmanager, added_on=added_on, amount = amount, note=note, transaction_type=category)

    return render(request, 'transaction.html', {'form': form})