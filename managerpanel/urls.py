from django.conf.urls import patterns, include, url
from managerpanel import views

urlpatterns = patterns('',
	url(r'^transaction/$', views.add_transaction, name='addtransaction'),
	url(r'^home/$', views.manager_panel, name='home'),
)