from django import forms
from django.forms import ModelForm, Textarea, RadioSelect

from .models import Transaction

class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        exclude = ['added_on',]
        widgets = {
        	'note': Textarea(attrs={'cols': 40, 'rows': 5}),
        }