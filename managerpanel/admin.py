from django.contrib import admin
from .models import Contact, ZManager, Transaction

admin.site.register(Contact)
admin.site.register(ZManager)
admin.site.register(Transaction)