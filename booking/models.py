from django.contrib.auth.models import User
from django.db import models
from allauth.account.models import EmailAddress
import datetime

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
 
    def __unicode__(self):
        return "{}'s profile".format(self.user.username)
 
    class Meta:
        db_table = 'user_profile'
 
    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False
    User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

class City(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    time = models.DateTimeField('time city')
    def __unicode__(self):
        return self.name

class Zostel(models.Model):
    city = models.OneToOneField(City)  #Use ForeignKeyField for multiple zostels in same city
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    address = models.TextField(max_length=500)
    email = models.EmailField(max_length=75)
    phone = models.CharField(max_length=20)
    time = models.DateTimeField('time zostel')
    def __unicode__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    time = models.DateTimeField('time category')
    def __unicode__(self):
        return self.name

class Room(models.Model):
    zostel = models.ForeignKey(Zostel)
    category = models.ForeignKey(Category)
    number = models.SmallIntegerField(max_length=3)
    name = models.CharField(max_length=100)
    beds = models.SmallIntegerField(max_length=3)
    
    STATUS = (
        ('AV', 'Available'),
        ('NA', 'Not Available'),
    )
    
    status = models.CharField(max_length=2, choices=STATUS, default='AV')
    price = models.SmallIntegerField(max_length=5)
    discount = models.SmallIntegerField(max_length=5)
    time = models.DateTimeField('time room')
    def final_price(self):
        return price - discount
    
    def __unicode__(self):
        return self.name

class Discount(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    token = models.CharField(max_length=10,unique=True)
    active = models.BooleanField()
    expire = models.DateTimeField('time discount')
    def __unicode__(self):
        return self.name
    
class Client(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=75)
    password = models.CharField(max_length=50)
    counter = models.IntegerField(max_length=5)
    loyality = models.IntegerField(max_length=5)
    time = models.DateTimeField('time client')
    def __unicode__(self):
        return self.name

class Booking(models.Model):
    client = models.ForeignKey(Client)
    name = models.CharField(max_length=20)
    address = models.TextField(max_length=500)
    email = models.EmailField(max_length=75)
    phone = models.CharField(max_length=20)
    success = models.BooleanField()
    time = models.DateTimeField('time booking')
    def __unicode__(self):
        return self.name

class BookingDetail(models.Model):
    booking = models.ForeignKey(Booking)
    room_id = models.ForeignKey(Room)
    number_bed = models.IntegerField(max_length=3)
    number_child = models.IntegerField(max_length=3)
    date_arrive = models.DateTimeField('date checkin')
    date_depart = models.DateTimeField('time checkout')
    time = models.DateTimeField('time booking_detail')
    def __unicode__(self):
        return str(self.booking)

class Gateway(models.Model):
    booking = models.ForeignKey(Booking)
    transaction = models.CharField(max_length=20)   #Transaction ID from Payment Gateway
    amount = models.IntegerField(max_length=5)
    def __unicode__(self):
        return self.transaction
    time = models.DateTimeField('time transaction')

class Payment(models.Model):
    booking = models.ForeignKey(Booking)
    discount = models.ForeignKey(Discount)
    gateway = models.ForeignKey(Gateway)
    total = models.IntegerField(max_length=5)
    deposited = models.IntegerField(max_length=5)

    MODE = (
        ('O', 'Online'),
        ('C', 'Cash'),        
    )
    mode = models.CharField(max_length=1, choices=MODE, default='O')
    time = models.DateTimeField('time payment')
    def remaining(self):
        return total - deposited