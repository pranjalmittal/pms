from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.template import Template, Context
from booking.models import *
import datetime
from django.utils import timezone
from django.db.models import Sum
from django.utils.html import escape


def book(request):
    cities = City.objects.all()
    return render(request, 'booking/form.html', {'cities': cities})

def availibility(request):
    params = {'city':request.GET['city'], 'arrival':request.GET['arrival'],
              'nights':request.GET['nights'], 'currency':request.GET.get('currency', 'INR')}
    
    date = datetime.datetime.strptime(params['arrival'], '%m/%d/%Y')
    nights = int(params['nights'])
    city = params['city']

    avail = available(date, city, nights)
    room_details = selectroom(date, city, nights)
    return render(request, 'booking/availability.html', {'available':avail, 'room_details':room_details, 'params':params})

# Returns availability for +/- 7 Days
def popupavail(request):
    params = {'city':request.GET['city'], 'arrival':request.GET['arrival'],
              'nights':request.GET['nights'], 'currency':request.GET.get('currency', 'INR')}
    
    date = datetime.datetime.strptime(params['arrival'], '%m/%d/%Y')
    nights = int(params['nights'])
    city = params['city']
    date = date - datetime.timedelta(days=7)
    nights = 15

    avail = available(date, city, nights)
    return render(request, 'booking/popupavail.html', {'available':avail, 'params':params})


# returns dictionary of number of available beds in each type of room category on provided date
def available(date, city, nights):
    available = {}
    date_temp = date

    cityname = City.objects.get(id=city).name
    print(cityname)
    available['cityname'] = cityname

    categories = Category.objects.all()
    category_name = []  #store the name of available categories

    for category in categories:
        category_name.append(category.name)
    available['category'] = category_name

    date_wise = []  #store date and availability per date in each category
    for i in range(nights):
        cat_temp = []
        cat_temp.append(date_temp.date())
        for category in categories:
            cat_temp.append(check(date_temp, city, category.name))
        date_wise.append(cat_temp)
        date_temp = date_temp + datetime.timedelta(days=1)
    available['available'] = date_wise
    return available

# Check number of available beds in each type of room category on provided date
def check(date, city, roomtype):
    booked = BookingDetail.objects\
                        .filter(date_arrive__lte=date, date_depart__gt=date)\
                        .filter(room_id__category__name=roomtype)\
                        .filter(room_id__zostel__city__id=city)\
                        .aggregate(Sum('number_bed'))\
                        .values()[0]
    booked = int(booked or 0)

    total = Room.objects.filter(category__name=roomtype)\
                        .filter(zostel__city__id=city)\
                        .aggregate(Sum('beds'))\
                        .values()[0]
    total = (total or 0)
    available = total - booked
    return available

# Return room's details on each date, availability
def selectroom(date, city, nights):
    rooms = Room.objects.filter(zostel__city__id=city)
    room_details = []
    for room in rooms:
        max_booking = []
        date_temp = date
        for i in range(nights):
            count = BookingDetail.objects\
                    .filter(room_id__id=room.id)\
                    .filter(date_arrive__lte=date_temp, date_depart__gt=date_temp)\
                    .aggregate(Sum('number_bed'))\
                    .values()[0]
            count = int(count or 0) #converting None to 0
            max_booking.append(count)
            date_temp = date_temp + datetime.timedelta(days=1)
        bookable = room.beds - max(max_booking)       
        bookable = range(bookable+1) # +1 is because in template it is rendered from 0 to n-1
        room_spec = {'id':room.id, 'name':room.name, 'category':room.category, 'beds':room.beds, 'price':room.price, 'bookable':bookable}
        room_details.append(room_spec)
    return room_details

#
def checkout(request):
    output = ''
    city = int(request.POST['city'])
    nights = int(request.POST['nights'])
    arrival = datetime.datetime.strptime(request.POST['arrival'], '%m/%d/%Y')
    departure = arrival + datetime.timedelta(days=nights)
    city_name = City.objects.get(id=city).name
    zostel_name = Zostel.objects.filter(city=city)[0].name
    
    info = {}
    info['arrival'] = arrival
    info['departure'] = departure
    info['nights'] = nights
    info['city'] = city_name
    info['zostel_name'] = zostel_name
    
    room_details = []
    total = 0
    
    for key, value in request.POST.iteritems():
        room_row = {}
        if(key[:4]=='room' and value!='0' ):
            room_name = Room.objects.get(id=int(key[5:])).name
            room_cost = Room.objects.get(id=int(key[5:])).price
            room_row['name'] = room_name
            room_cat = Room.objects.get(id=int(key[5:])).category.name
            room_row['category'] = room_cat
            room_row['people'] = value
            room_row['cost'] = room_cost
            room_row['total'] = int(room_cost) * int(value)
            total += int(room_row['total'])
            room_details.append(room_row)
    
    info['room_details'] = room_details
    info['total'] = total
    info['deposite'] = 0.0025 * total
    info['fee'] = 0
    info['paid'] = info['deposite']
    info['balance'] = total - int(info['deposite'])

    return render(request, 'booking/checkout.html', {'info':info})