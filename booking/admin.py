from django.contrib import admin
from booking.models import UserProfile, City, Zostel, Category, Room, Discount, Client, Booking, BookingDetail, Gateway, Payment

admin.site.register(UserProfile)
admin.site.register(City)
admin.site.register(Zostel)
admin.site.register(Category)
admin.site.register(Room)
admin.site.register(Discount)
admin.site.register(Client)
admin.site.register(Booking)
admin.site.register(BookingDetail)
admin.site.register(Gateway)
admin.site.register(Payment)