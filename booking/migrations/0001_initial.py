# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table('user_profile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='profile', unique=True, to=orm['auth.User'])),
        ))
        db.send_create_signal(u'booking', ['UserProfile'])

        # Adding model 'City'
        db.create_table(u'booking_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['City'])

        # Adding model 'Zostel'
        db.create_table(u'booking_zostel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['booking.City'], unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Zostel'])

        # Adding model 'Category'
        db.create_table(u'booking_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Category'])

        # Adding model 'Room'
        db.create_table(u'booking_room', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('zostel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Zostel'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Category'])),
            ('number', self.gf('django.db.models.fields.SmallIntegerField')(max_length=3)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('beds', self.gf('django.db.models.fields.SmallIntegerField')(max_length=3)),
            ('status', self.gf('django.db.models.fields.CharField')(default='AV', max_length=2)),
            ('price', self.gf('django.db.models.fields.SmallIntegerField')(max_length=5)),
            ('discount', self.gf('django.db.models.fields.SmallIntegerField')(max_length=5)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Room'])

        # Adding model 'Discount'
        db.create_table(u'booking_discount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('token', self.gf('django.db.models.fields.CharField')(unique=True, max_length=10)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('expire', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Discount'])

        # Adding model 'Client'
        db.create_table(u'booking_client', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('counter', self.gf('django.db.models.fields.IntegerField')(max_length=5)),
            ('loyality', self.gf('django.db.models.fields.IntegerField')(max_length=5)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Client'])

        # Adding model 'Booking'
        db.create_table(u'booking_booking', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Client'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Booking'])

        # Adding model 'BookingDetail'
        db.create_table(u'booking_bookingdetail', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('booking', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Booking'])),
            ('room_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Room'])),
            ('number_bed', self.gf('django.db.models.fields.IntegerField')(max_length=3)),
            ('number_child', self.gf('django.db.models.fields.IntegerField')(max_length=3)),
            ('date_arrive', self.gf('django.db.models.fields.DateTimeField')()),
            ('date_depart', self.gf('django.db.models.fields.DateTimeField')()),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['BookingDetail'])

        # Adding model 'Gateway'
        db.create_table(u'booking_gateway', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('booking', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Booking'])),
            ('transaction', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('amount', self.gf('django.db.models.fields.IntegerField')(max_length=5)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Gateway'])

        # Adding model 'Payment'
        db.create_table(u'booking_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('booking', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Booking'])),
            ('discount', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Discount'])),
            ('gateway', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['booking.Gateway'])),
            ('total', self.gf('django.db.models.fields.IntegerField')(max_length=5)),
            ('deposited', self.gf('django.db.models.fields.IntegerField')(max_length=5)),
            ('mode', self.gf('django.db.models.fields.CharField')(default='O', max_length=1)),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'booking', ['Payment'])


    def backwards(self, orm):
        # Deleting model 'UserProfile'
        db.delete_table('user_profile')

        # Deleting model 'City'
        db.delete_table(u'booking_city')

        # Deleting model 'Zostel'
        db.delete_table(u'booking_zostel')

        # Deleting model 'Category'
        db.delete_table(u'booking_category')

        # Deleting model 'Room'
        db.delete_table(u'booking_room')

        # Deleting model 'Discount'
        db.delete_table(u'booking_discount')

        # Deleting model 'Client'
        db.delete_table(u'booking_client')

        # Deleting model 'Booking'
        db.delete_table(u'booking_booking')

        # Deleting model 'BookingDetail'
        db.delete_table(u'booking_bookingdetail')

        # Deleting model 'Gateway'
        db.delete_table(u'booking_gateway')

        # Deleting model 'Payment'
        db.delete_table(u'booking_payment')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'booking.booking': {
            'Meta': {'object_name': 'Booking'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Client']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'booking.bookingdetail': {
            'Meta': {'object_name': 'BookingDetail'},
            'booking': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Booking']"}),
            'date_arrive': ('django.db.models.fields.DateTimeField', [], {}),
            'date_depart': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number_bed': ('django.db.models.fields.IntegerField', [], {'max_length': '3'}),
            'number_child': ('django.db.models.fields.IntegerField', [], {'max_length': '3'}),
            'room_id': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Room']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'booking.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'booking.city': {
            'Meta': {'object_name': 'City'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'booking.client': {
            'Meta': {'object_name': 'Client'},
            'counter': ('django.db.models.fields.IntegerField', [], {'max_length': '5'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loyality': ('django.db.models.fields.IntegerField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'booking.discount': {
            'Meta': {'object_name': 'Discount'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'expire': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'})
        },
        u'booking.gateway': {
            'Meta': {'object_name': 'Gateway'},
            'amount': ('django.db.models.fields.IntegerField', [], {'max_length': '5'}),
            'booking': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Booking']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {}),
            'transaction': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'booking.payment': {
            'Meta': {'object_name': 'Payment'},
            'booking': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Booking']"}),
            'deposited': ('django.db.models.fields.IntegerField', [], {'max_length': '5'}),
            'discount': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Discount']"}),
            'gateway': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Gateway']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mode': ('django.db.models.fields.CharField', [], {'default': "'O'", 'max_length': '1'}),
            'time': ('django.db.models.fields.DateTimeField', [], {}),
            'total': ('django.db.models.fields.IntegerField', [], {'max_length': '5'})
        },
        u'booking.room': {
            'Meta': {'object_name': 'Room'},
            'beds': ('django.db.models.fields.SmallIntegerField', [], {'max_length': '3'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Category']"}),
            'discount': ('django.db.models.fields.SmallIntegerField', [], {'max_length': '5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'number': ('django.db.models.fields.SmallIntegerField', [], {'max_length': '3'}),
            'price': ('django.db.models.fields.SmallIntegerField', [], {'max_length': '5'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'AV'", 'max_length': '2'}),
            'time': ('django.db.models.fields.DateTimeField', [], {}),
            'zostel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['booking.Zostel']"})
        },
        u'booking.userprofile': {
            'Meta': {'object_name': 'UserProfile', 'db_table': "'user_profile'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'profile'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'booking.zostel': {
            'Meta': {'object_name': 'Zostel'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'city': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['booking.City']", 'unique': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['booking']