from django.conf.urls import patterns, include, url
from booking import views

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),   # default django admin
    url(r'^accounts/', include('allauth.urls')), # allauth urls
    url(r'^book/$', views.book, name='book'),
    url(r'^availibility/$', views.availibility, name='availibility'),
    url(r'^popupavail/$', views.popupavail, name='popupavail'),
    url(r'^checkout/$', views.checkout, name='checkout'),
    url(r'^managerpanel/', include('managerpanel.urls')),
)